        // Array of image URLs
        const imageUrls = [
             '../images/3VhtaM.png',
			'../images/8-bit-japan-2304-x-1536-x33n0xe5x0e00t9w.jpg',
			'../images/94b18dfe260d409abe25d7f2cf21e793.jpg',
            // Add more image URLs as needed
        ];

        function selectRandomImage() {
            const randomIndex = Math.floor(Math.random() * imageUrls.length);
            const randomImageUrl = imageUrls[randomIndex];
            
            const imgElement = document.getElementById('randomImage');
            imgElement.src = randomImageUrl;
        }
