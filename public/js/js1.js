
// Modal Image Gallery
function onClick(element) {
  document.getElementById("img01").src = element.src;
  document.getElementById("modal01").style.display = "block";
  var captionText = document.getElementById("caption");
  captionText.innerHTML = element.alt;
}

// Change style of navbar on scroll
window.onscroll = function() {myFunction()};
function myFunction() {
    var navbar = document.getElementById("myNavbar");
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        navbar.className = "w3-bar" + " w3-card" + " w3-animate-top" + " w3-white";
    } else {
        navbar.className = navbar.className.replace(" w3-card w3-animate-top w3-white", "");
    }
}

// Used to toggle the menu on small screens when clicking on the menu button
function toggleFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

// Used to pause and play music for the "MUSIC" button
var myMusic= document.getElementById("music");
function play() {
	return music.paused ? music.play() : music.pause();
	
}

// Function to select a random class and apply it to the "home" section on page load
    window.addEventListener("DOMContentLoaded", function () {
      const classArray = ['bgimg-1-0', 'bgimg-1-1', 'bgimg-1-2', 'bgimg-1-3'];
      const randomIndex = Math.floor(Math.random() * classArray.length);
      const randomClass = classArray[randomIndex];
      document.getElementById("home").classList.add(randomClass);
    });

    // Your other JavaScript functions and code can go here

    // Example of a function to open the form (from your code)
    function openForm() {
      document.getElementById("myForm").style.display = "block";
    }

    // Example of a function to close the form (from your code)
    function closeForm() {
      document.getElementById("myForm").style.display = "none";
    }

function toggleDarkMode() {
  document.body.classList.toggle("dark-mode");
}
