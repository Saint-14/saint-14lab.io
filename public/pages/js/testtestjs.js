// Array of image URLs
const imageUrls = [
    '../images/3VhtaM',
    '../images/224357-No_Mans_Sky-video_games-fantasy_art-concept_art-science_fiction',
    '../images/487138',
    // Add more image URLs as needed
];

function selectRandomImage() {
    const randomIndex = Math.floor(Math.random() * imageUrls.length);
    const randomImageUrl = imageUrls[randomIndex];
    
    const imgElement = document.getElementById('randomImage');
    imgElement.src = randomImageUrl;
}
